class Solution:
    def islandPerimeter(self, grid: List[List[int]]) -> int:

        m = len(grid)
        n = len(grid[0])
        kq = 0
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 0:
                    kq += 0 
                if grid[i][j] == 1:
                    kq += 4
                    if j>0 and grid[i][j-1] == 0:
                        kq -= 0
                    if j>0 and grid[i][j-1] == 1:
                        kq -= 1
                    if j < n-1 and grid[i][j+1] == 0:
                        kq -= 0
                    if j < n-1 and grid[i][j+1] == 1:
                        kq -= 1              
                    if i>0 and grid[i-1][j] == 0:
                        kq -= 0               
                    if i>0 and grid[i-1][j] == 1:
                        kq -= 1
                    if i < m-1 and grid[i+1][j] == 0:
                        kq -= 0               
                    if i < m-1 and grid[i+1][j] == 1:
                        kq -= 1
        return kq