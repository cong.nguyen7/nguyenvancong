class Solution:
    def trimBST(self, root: Optional[TreeNode], low: int, high: int) -> Optional[TreeNode]:
        if not root:
            return None
        if root.val < low:
            # loại bỏ gốc và cây con bên trái của nó
            root = self.trimBST(root.right, low, high)
        elif root.val > high:
            # loại bỏ gốc và cây con bên phải của nó
            root = self.trimBST(root.left, low, high)
        else:
            # giữ gốc và cắt cây con bên trái và bên phải
            root.left = self.trimBST(root.left, low, high)
            root.right = self.trimBST(root.right, low, high)
        return root