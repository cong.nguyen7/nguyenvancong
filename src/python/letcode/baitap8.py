class Solution:
    def calPoints(self, ops: List[str]) -> int:
        lis=[]
        for i in ops:
            if(i=="D"):
                    lis.append(lis[-1]*2) #nhân đôi của giá trị cuối cùng
            elif(i=="C"):
                lis.pop()    #loại bỏ phần tử cuối cùng
            elif(i=="+"):
                lis.append(lis[-1]+lis[-2])  #lưu trữ tổng của phần tử cuối cùng và cuối cùng thứ hai
            else:
                lis.append(int(i)) #lưu trữ giá trị trong một danh sách
        return sum(lis)  