class MinStack:
    def __init__(self):
        self.stack=deque([]) 		#Khởi tạo cấu trúc dữ liệu
        self.min=deque([])    
    def push(self, val: int) -> None:        # Đẩy phần tử val lên ngăn xếp.
        self.stack.append(val) 		
        if len(self.min)==0 or val<self.min[-1]:
            self.min.append(val)
        else:
            self.min.append(self.min[-1])
    def pop(self) -> None:			#Loại bỏ phần tử trên cùng của ngăn xếp và trả về phần tử đó.
        x=self.stack.pop()
        self.min.pop()
        return x
    def top(self) -> int:			#Nhận phần tử hàng đầu.
        return self.stack[-1]
    def getMin(self) -> int:
        return self.min[-1]
