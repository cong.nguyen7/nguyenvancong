class Solution:
    def distributeCandies(self, candyType: List[int]) -> int:
        s = set(candyType)       #Các phần tử trong set không cho phép lặp lại
        dem = len(candyType)//2  #Chia lấy phần nguyên 
        if len(s) >= dem:        # so sánh phần tử trong mảng s và biến đếm
            return dem
        return len(s)
    

        # return min(len(set(candyType)), len(candyType)//2)