class Solution():
    def lengthOfLongestSubstring(self, s):
        i = j = ketqua = 0    #xác định kích thước cửa sổ trượt #ketqua để lưu trữ độ dài chuổi con dài nhất 
        chars = set()         #khởi tạo tập hợp chars không có kí tự trùng hợp
        while j < len(s):
            #Nếu ký tự hiện tại đã xuất hiện trước đó (không có trong tập hợp ), thêm ký tự vào tập hợp và tăng j và cũng cập nhật biến “ketqua” lưu trữ câu trả lời.
            if s[j] not in chars:
                chars.add(s[j])
                j = j + 1
                ketqua = max(ketqua, len(chars))
                #Ngược lại, nếu ký tự hiện tại đã được lặp lại (có trong tập hợp) tại một chỉ mục trước đó i, hãy đặt “ketqua” làm độ dài hiện tại của cửa sổ trượt và xóa ký tự tại chỉ mục i, tức là s[i]
            else:
                chars.remove(s[i])
                i = i + 1
        return ketqua
    
#Sliding Window  #0(m^2)  #abcabcbb
