class Solution:
    def numJewelsInStones(self, Jewels: str, Stones: str) -> int:
        dem = 0             # khởi tạo và gán biến đếm = 0
        for item in Stones: #lặp các kí tự trong chuổi Stones 
            if item in Jewels:   #nếu có kí tự lặp lại trong chuổi j thì biến đếm = biến đếm + 1
                dem += 1    
        return dem
    
        # return sum([1 for jewel in stones if jewel in jewels])
