class Solution:
    def mergeTrees(self, root1: Optional[TreeNode], root2: Optional[TreeNode]) -> Optional[TreeNode]:

        if root1 and root2: 
            # tạo một nút mới (gốc) và Hợp nhất các giá trị từ hai cây
            node = TreeNode(root1.val + root2.val)
            # Hợp nhất các cây bên trái bằng cách sử dụng hàm đệ quy
            node.left = self.mergeTrees(root1.left, root2.left)
            # Hợp nhất các cây bên phải bằng cách sử dụng hàm đệ quy
            node.right = self.mergeTrees(root1.right, root2.right)
            
            return node
            
        else:
            return root1 or root2  